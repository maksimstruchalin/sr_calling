Written by Makism Struchalin for Appliedgenomics
date May 14, 2021


How to install:
Run ./install.sh

How to run:
./run.sh <reference_genome_filename> <path_to_directory_with_BAM_files>

e.g. ./run.sh /home/maksim/genome_reference_h19/hg19_22xy.fa /home/maksim/SV/



Log file is in SR.log


