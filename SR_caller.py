import os, fnmatch
import pickle
import pandas as pd
import subprocess
import allel
from datetime import datetime
import warnings
warnings.filterwarnings("ignore")

import sys, getopt

#run bash command and logging output
#________________________________________
def run_shell(shell_command, log_file, msg):
    
    try:
        f1=open(log_file, 'a')
        stdout = subprocess.check_output(shell_command, shell=True, stderr=f1)
        status = 0
        f1.write(datetime.now().strftime("%d/%m/%Y %H:%M:%S") + ': ')
        f1.write(msg)
        f1.write(stdout)
        f1.write('\n')
        f1.close()
    except:
        status=1

    return status
#________________________________________


#Get input arguments
#________________________________________

exec_file_name = 'SR_caller.py'

gen_ref_file = ''
sample_dir = ''



if( len(sys.argv) != 3):
    print('Specify input parameters:')
    print('./run.sh --reference=<genome_reference> --sample_dir=<dir_with_bam_files>')
    print('e.g.: ./run.sh --reference=/home/maksim/genome_reference_h19/hg19_22xy.fa --sample_dir=/home/maksim/SV/')
    exit(1)

try:
   opts, args = getopt.getopt(sys.argv[1:],"hr:s:",["reference=","sample_dir="])
except getopt.GetoptError:
   print('%s --reference=<genome_reference> --sample_dir=<dir_with_bam_files>' % exec_file_name)
   sys.exit(2)
for opt, arg in opts:
   if opt == '-h':
      print('Run on Linux only\n%s --reference=<genome_reference> --sample_dir=<dir_with_bam_files>' % exec_file_name)
      sys.exit()
   elif opt in ("-r", "--reference"):
      gen_ref_file = arg
   elif opt in ("-s", "--sample_dir"):
      sample_dir = arg
print('Genome reference is %s' % gen_ref_file)
print('Directory with sample BAM files is %s' % sample_dir)
#________________________________________






#Initialization
#________________________________________
root = os.getcwd()#'/home/maksim/package/'
data_dir = sample_dir#root + 'data/'
data_dir_output = root +'/results/'
data_dir_tmp = data_dir_output + 'data_tmp/'

log_file = 'SR.log'
results_dir = data_dir_output + 'manta_output/'

genome_assembly_filename = str.split(gen_ref_file,  '/')[-1]#'hg19_22xy.fa'
reference_dir = gen_ref_file.replace(genome_assembly_filename, '')#root + '/reference/'


SR_info_filename = 'indels.info'

#size around structural variance +-500bp
flank_window_size = 500 # bp
region_filename = 'regions.bed'

minimum_sr_size = 10

SR_res_filename = results_dir + 'results/variants/diploidSV.vcf.gz'


sample_name_str = subprocess.check_output('cd %s ; ls *bam' % data_dir , shell=True)
sample_names = str.split(sample_name_str, '\n')[:-1]
print('Found the following sample files:')
for sample in sample_names:
    print(sample)

print('\n')

SR_res_ready_vec = [] # final results store here
#________________________________________




#clean all
#________________________________________
subprocess.check_output('rm -rf  %s'%log_file, shell=True)

shell_command = 'rm -rf %s/*' % data_dir_output

msg = 'removed previous manta configuration sucessfully'
status = run_shell(shell_command, root + '/' + log_file, msg)

if(status == 0): print(msg)
else: 
    print('error: can not removed previous manta configuration')
    print(shell_command)
    exit(1) 
#________________________________________





#create tmp data dir and links to bam files
#________________________________________

shell_command = 'rm -rf %s ; mkdir %s' % (data_dir_tmp, data_dir_tmp)

msg = 'created %s sucessfully ' % data_dir_tmp
status = run_shell(shell_command, root + '/' + log_file, msg)

if(status == 0): print(msg)
else: 
    print('error: can not created %s ' % data_dir_tmp)
    print(shell_command)
    exit(1) 

    
shell_command = 'cd %s ; ln -sf %s/*.bam .' % (data_dir_tmp, data_dir)
msg = 'links to bam files sucessfully'
status = run_shell(shell_command, root + '/' + log_file, msg)

if(status == 0): print(msg)
else: 
    print('error: can not created links to bam files')
    print(shell_command)
    exit(1) 
#________________________________________



#Create bed file with info about structural variancves we are interesed in. Need for the later variant calling in these regions only.
#________________________________________
SR_info = pd.read_csv(data_dir + SR_info_filename, sep='\t').sort_values(['Chromosome', 'Locus (start position)'])
regions = SR_info[['Chromosome']].copy()
regions['Chromosome'] = regions['Chromosome'].astype(str)
regions['Chromosome'] = 'chr' + regions['Chromosome']
regions['start'] = SR_info['Locus (start position)'] - flank_window_size
regions['end'] = SR_info['Locus (start position)'] + flank_window_size
regions.to_csv(data_dir_output + region_filename, sep='\t', index=None, header=False)
print('File with regions saved into %s'%data_dir_output + region_filename)
#________________________________________


#build index file for regions.bed
#________________________________________
shell_command = 'cd %s ; cat %s | bgzip > %s.bgz' % (data_dir_output, region_filename, region_filename)
msg = 'bgzip sucessfully archived file ' + data_dir_output+region_filename
status = run_shell(shell_command, root + '/' + log_file, msg)

if(status == 0): print(msg)
else: 
    print('error: bgzip can not archive file ' + data_dir_output+region_filename)
    exit(1) 

    
shell_command = 'cd %s ; tabix -p bed %s.bgz'%(data_dir_output, region_filename)
msg = 'tabix sucessfully indexed file %s.bgz' % (data_dir_output+region_filename)
status = run_shell(shell_command, root + '/' + log_file, msg)

if(status == 0): print(msg)
else: 
    print('error: tabix can not index file %s.bgz' % (data_dir_output+region_filename))
    exit(1) 
#________________________________________




#________________________________________
#check weather index files exist for sample files or create it
bam_filenames = fnmatch.filter(os.listdir(data_dir), '*.bam')

for bam_filename in bam_filenames:
    bam_filename_path = data_dir_tmp + bam_filename
    
    if(not os.path.isfile(bam_filename_path + '.bai')):
    
        shell_command = 'samtools index ' + bam_filename_path
        msg = 'samtools sucessfully indexed file ' + bam_filename_path
        status = run_shell(shell_command, root + '/' + log_file, msg)
        
        if(status == 0): print(msg)
        else:
            print('error: samtools can not index file ' + bam_filename_path)
            exit(1) 
    else:
        print('Index file for ' + bam_filename_path + ' successfully found.')
#________________________________________


#check weather index files exist for reference genome
#________________________________________

genome_assembly_index_filename = reference_dir + genome_assembly_filename+'.fai'

if(not os.path.isfile(genome_assembly_index_filename)):
    shell_command = 'cd %s ; samtools faidx %s' % (reference_dir, genome_assembly_filename)
    msg = 'samtools sucessfully indexed file ' + genome_assembly_filename
    status = run_shell(shell_command, root + '/' + log_file, msg)
    
    if(status == 0): print(msg)
    else:
        print('error: samtools can not index file ' + genome_assembly_filename)
        exit(1) 
    
else:
    print('Index file for ' + genome_assembly_filename + ' successfully found.')
#________________________________________



#Process SR calling
#________________________________________


for sample_name in  sample_names:
    
    print('\nStart SR calling for %s' % sample_name)
    #Configure manta
    sample_file = data_dir_tmp + sample_name
    
    reference_file = reference_dir + genome_assembly_filename
    region_file = data_dir_output+region_filename
    
    #remove old configuration
    shell_command = 'rm -rf %s' % results_dir
   
    msg = 'removed old configuration sucessfully'
    status = run_shell(shell_command, root + '/' + log_file, msg)
    
    if(status == 0): print(msg)
    else: 
        print('error: can nor removed old configuration')
        print(shell_command)
        exit(1) 
    
    
    shell_command = 'configManta.py --generateEvidenceBam --outputContig --exome --bam %s --referenceFasta %s --runDir %s --callRegions %s.bgz --generateEvidenceBam --outputContig' % (sample_file, reference_file, results_dir, region_file)
    msg = 'configManta.py run sucessfully'
    status = run_shell(shell_command, root + '/' + log_file, msg)
    if(status == 0): print(msg)
    else:
        print('error: configManta.py fineshed with error ')
        print(shell_command)
        exit(1) 


    
    #Edit manta config file
    config_file = results_dir + 'runWorkflow.py.config.pickle'
    infile = open(config_file,'rb')
    new_dict = pickle.load(infile)
    new_dict['manta']['minScoredVariantSize'] = minimum_sr_size
    pickle.dump(new_dict, open(config_file, 'wb' ) )


    #Run calling 
    shell_command = 'results/manta_output/runWorkflow.py &>> %s' % log_file # % (root, root + '/' + log_file)
    msg = 'manta run sucessfully'
    status = run_shell(shell_command, root + '/' + log_file, msg)
    if(status == 0): print(msg)
    else:
        print('error: manta fineshed with error. See log file %s'%log_file)
        print(shell_command)
        exit(1) 


    #read results
    sample_res_filename = data_dir_output+'/' + sample_name.replace('.bam','') + '_manta_result.vcf.gz'
    #copy reslt file
    shell_command = 'cp %s %s' % (SR_res_filename, sample_res_filename)
    msg = 'Original results files copied results sucessfully'
    status = run_shell(shell_command, root + '/' + log_file, msg)
    
    if(status == 0): print(msg)
    else:
        print('error: can not copy results. See log file %s'%log_file)
        print(shell_command)
        exit(1) 
    
    SR_res = allel.vcf_to_dataframe(sample_res_filename)
    number_of_alleles_ = allel.read_vcf(SR_res_filename)['calldata/GT']
    number_of_alleles = [x[0][0]+x[0][1] for x in number_of_alleles_]
    SR_res['number_of_alleles'] = number_of_alleles
    
    cols = pd.Series(SR_res.columns)
    cols.loc[cols == 'POS'] = 'SR_start_position_manta'
    cols.loc[cols == 'CHROM'] = 'Chromosome'
    cols.loc[cols == 'ID'] = 'SR_type'
    cols.loc[cols == 'ALT_1'] = 'ALT_manta'
    cols.loc[cols == 'REF'] = 'REF_manta'
    SR_res.columns = cols
    
    #get rid of chr prefix
    SR_res['Chromosome'] = SR_res['Chromosome'].str.replace('chr','')
    SR_res['Chromosome'] = SR_res['Chromosome'].astype(int)
    
    mutation_type = dict()
    mutation_type[0] = 'WT'
    mutation_type[1] = 'het'
    mutation_type[2] = 'hom'
    
    SR_res['Classification'] = [mutation_type[x] for x in SR_res['number_of_alleles']]
    SR_res['additiona_info'] = sample_res_filename
    
   
    mutation_number = [int(x.split('#')[1]) for x in SR_info['ID']]
    SR_info['mutation_number'] = mutation_number
    SR_info.sort_values('mutation_number', inplace=True)

    SR_res['ID'] = None

    for index, row in SR_res.iterrows():
    
        f = (SR_info['Chromosome'] == row['Chromosome']) 
        SR_info_cut = SR_info.loc[f].copy()
        SR_info_cut['pos_diff'] = SR_info_cut['Locus (start position)'] - row['SR_start_position_manta']
        close_mutation_index = SR_info_cut['pos_diff'].abs().idxmin()
    
        SR_res['ID'].loc[index] = SR_info.loc[close_mutation_index]['ID']
    
    SR_res_sample = pd.merge(SR_info, SR_res, how='outer', on=['Chromosome', 'ID']).copy()
    SR_res_sample['Sample'] = sample_name.replace('.bam','')
    
    cols = pd.Series(SR_res_sample.columns)
    cols.loc[cols == 'Locus (start position)'] = 'SR_start_position'
    SR_res_sample.columns = cols
    
    f = SR_res_sample['Classification'].isnull()
    SR_res_sample['Classification'].loc[f] = 'WT'
    # SR_res_sample['REF'] = SR_res_sample['Reference'].loc[f]
    # SR_res_sample['ALT'] = SR_res_sample['Alternate'].loc[f]
    
    
    SR_res_sample_ready = SR_res_sample[['Sample','ID','Chromosome','SR_start_position', 'SR_start_position_manta','REF_manta', 'Reference','ALT_manta', 'Alternate','Classification','Gene','SR_type','additiona_info']]
    
    SR_res_ready_vec.append(SR_res_sample_ready)


final_res = pd.concat(SR_res_ready_vec)
final_res_filename = root + '/SR_calling_results.csv'
final_res.to_csv(final_res_filename, index=None)

print("All done!")
print("File with results saved in %s\n\n" % final_res_filename)
print('columns in the file mean the following:')
print('Sample - name of the sample which bam file corresponds to')
print('ID - name of mutation')
print('Chromosome - Chromosome')
print('SR_start_position - start position of structural variant as in file %s' % data_dir + SR_info_filename)
print('SR_start_position_manta - start position of structural variant discovered by manta caller')
print('REF_manta - reference allele from manta')
print('Reference - reference allele from %s' % data_dir + SR_info_filename)
print('ALT_manta - alternative allele from manta')
print('Alternate - alternative allele from %s' % data_dir + SR_info_filename)
print('Classification - state of genotype (heterozygote, wild type, homozygote)')
print('Gene - gene name')
print('SR_type - what type of a structural varinat according to manta')
print('additiona_info - name of file with additional info')







